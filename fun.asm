section .data
	fmt db "1 + 2 = %d", 10, 0

section .code
	extern printf
	global main
main:
	push ebp
	mov ebp, esp

	push 1
	push 2
	call add
	add esp, 8

	push eax
	push fmt
	call printf
	add esp, 8
	
	mov esp, ebp
	pop ebp
	ret

add:
	push ebp
	mov ebp, esp

	mov eax, dword [ebp+8]
	mov ebx, dword [ebp+12]
	add eax,ebx

	mov esp,ebp
	pop ebp
	ret
